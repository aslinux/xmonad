import XMonad
import qualified XMonad.Actions.Search as S
import XMonad.Actions.CopyWindow (kill1)
import XMonad.Actions.WithAll (killAll)
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers (doCenterFloat)
import XMonad.Hooks.StatusBar
import XMonad.Hooks.WindowSwallowing (swallowEventHook)
import XMonad.Layout.LayoutModifier (ModifiedLayout)
import XMonad.Layout.NoBorders (noBorders, smartBorders)
import XMonad.Layout.Renamed
import XMonad.Layout.ResizableTile
import XMonad.Layout.Spacing
import XMonad.Prompt
import XMonad.Prompt.Man
import XMonad.Prompt.Shell (shellPrompt)
import qualified XMonad.StackSet as W
import XMonad.Util.EZConfig (additionalKeysP)
import XMonad.Util.NamedScratchpad
import System.Exit (exitSuccess)

-- define variables
myModMask :: KeyMask
myModMask = mod4Mask

myFont :: String
myFont = "xft:UbuntuMono Nerd Font:size=16:style=Regular:antialias=true:hinting=true"

myTerminal :: String
myTerminal = "alacritty"

myBrowser :: String
myBrowser = "io.gitlab.librewolf-community"

myBrowser' :: String
myBrowser' = "com.github.Eloston.UngoogledChromium"

myEditor :: String
myEditor = "emacsclient -c -a 'emacs' "

--myEditor = myTerminal ++ " -e nvim "

myFM :: String
myFM = "pcmanfm"

myBorderWidth :: Dimension
myBorderWidth = 3

mynormalBorderColor, myfocusedBorderColor :: String
mynormalBorderColor = "gray"
myfocusedBorderColor = "red"

scratchpads :: [NamedScratchpad]
scratchpads =
  [ NS "Terminal" spawnTerm nameTerm geometryTerm,
    NS "FileManager" myFM (className =? "Pcmanfm") (customFloating $ W.RationalRect (1 / 6) (1 / 6) (2 / 3) (2 / 3))
  ]
  where
    spawnTerm = myTerminal ++ " -t scratchpad"
    nameTerm = title =? "scratchpad"
    geometryTerm = customFloating $ W.RationalRect l t w h
      where
        h = 0.5
        w = 0.5
        t = 0.25
        l = 0.25

archwiki, odysee, startpage :: S.SearchEngine
archwiki = S.searchEngine "archwiki" "https://wiki.archlinux.org/index.php?search="
odysee = S.searchEngine "odysee" "https://odysee.com/$/search?q="
startpage = S.searchEngine "startpage" "https://www.startpage.com/sp/search?query="

searchList :: [(String, S.SearchEngine)]
searchList =
  [ ("a", archwiki),
    ("o", odysee),
    ("s", startpage)
  ]

main :: IO ()
main =
  xmonad
    . ewmhFullscreen
    . ewmh
    . withSB mySB
    . docks
    $ def
      { terminal = myTerminal,
        modMask = myModMask,
        borderWidth = myBorderWidth,
        normalBorderColor = mynormalBorderColor,
        focusedBorderColor = myfocusedBorderColor,
        handleEventHook = myhandleEventHook,
        manageHook = myManageHook <+> manageDocks,
        layoutHook = myLayout
      }
      `additionalKeysP` ( [ ("M-S-x", io exitSuccess),
                            ("M-S-q", kill1),
                            ("M-C-q", killAll),
                            ("M-S-r", spawn "xmonad --restart"),
                            ("M-C-r", spawn "xmonad --recompile"),
                            ("M-C-l", spawn "betterlockscreen -l"),
                            ("M-C-s", spawn "systemctl suspend"),
                            ("M-m", manPrompt myPromptConfig),
                            ("M-r", shellPrompt myPromptConfig),
                            ("M-t", namedScratchpadAction scratchpads "Terminal"),
                            ("M-f", namedScratchpadAction scratchpads "FileManager"),
                            ("M-d", spawn "rofi -show drun"),
                            ("M-<Tab>", sendMessage NextLayout),
                            ("M-<Space>", sendMessage ToggleStruts),
                            ("M-<Return>", spawn (myTerminal)),
                            ("M-b", spawn (myBrowser)),
                            ("M-<R>", windows W.focusDown),
                            ("M-<L>", windows W.focusUp),
                            ("M-S-<R>", windows W.swapDown),
                            ("M-S-<L>", windows W.swapUp),
                            ("M-<KP_Add>", sendMessage Expand),
                            ("M-<KP_Subtract>", sendMessage Shrink),
                            ("<XF86AudioRaiseVolume>", spawn "amixer set Master 5%+ unmute"),
                            ("<XF86AudioLowerVolume>", spawn "amixer set Master 5%- unmute"),
                            ("<XF86AudioMute>", spawn "amixer set Master toggle")
                          ]
                            ++ [("M-s " ++ k, S.promptSearchBrowser myPromptConfig myBrowser' f) | (k, f) <- searchList]
                            ++ [("M-S-s " ++ k, S.selectSearchBrowser myBrowser' f) | (k, f) <- searchList]
                        )

myhandleEventHook = swallowEventHook (title =? "Alacritty") (return True)

myManageHook :: ManageHook
myManageHook =
  composeAll
    [ className =? "Lxappearance" --> doCenterFloat,
      className =? "Lxpolkit" --> doCenterFloat,
      className =? "Sxiv" --> doCenterFloat,
      title =? "as-xmobar" --> doCenterFloat,
      className =? "Nitrogen" --> doCenterFloat
    ]
    <+> namedScratchpadManageHook scratchpads

myLayout = avoidStruts $ myDefaultLayout
  where
    myDefaultLayout =
      smartBorders tall
        ||| noBorders Full

tall =
  renamed [Replace "tall"] $
    smartBorders $
      mySpacing 10 $
        ResizableTall 1 (5 / 100) (55 / 100) []

mySpacing :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing i = spacingRaw False (Border i i i i) True (Border i i i i) True

myPromptConfig :: XPConfig
myPromptConfig =
  def
    { font = myFont,
      bgColor = "#282828",
      fgColor = "#ebdbb2",
      position =
        CenteredAt
          { xpCenterY = 0.0175,
            xpWidth = 0.955
          },
      height = 30,
      borderColor = "#282828",
      historySize = 3
    }

mySB :: StatusBarConfig
mySB = statusBarProp "xmobar ~/.config/xmonad/xmobarrc --alpha 180" (pure (filterOutWsPP [scratchpadWorkspaceTag] $ myXmobarPP))

myXmobarPP :: PP
myXmobarPP =
  def
    { ppSep = " • ",
      ppCurrent = xmobarColor "#458588" "",
      ppOrder = \(ws : l : t) -> [ws, l]
    }
